﻿// Copyright 2018 Laurens Mathot (Code Animo)
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using UnityEngine;

namespace CodeAnimo
{
	public class TrackerPhysics : MonoBehaviour {
		protected FixedJoint attachment;
		protected FixedJoint trackerConnection;

		protected TriggerColliderTransition colliderTransitions;

		protected void Reset()
		{
			if (GetComponent<Rigidbody>() == null)
			{
				Rigidbody rb = gameObject.AddComponent<Rigidbody>();
				rb.mass = 0.2f;// Weight of the HTC Vive controllers in real life is roughly 200gram.
			}
			if (GetComponent<TriggerColliderTransition>() == null)
			{
				gameObject.AddComponent<TriggerColliderTransition>();
			}
		}

		public void Awake()
		{
			colliderTransitions = GetComponent<TriggerColliderTransition>();
		}

		private void OnJointBreak(float breakForce)
		{
			if (attachment == null)	{ colliderTransitions.makeColliderWhenFree = true; }
		}

		public void ConnectToTracker(Rigidbody kinematicObject)
		{
			if (trackerConnection == null) { trackerConnection = gameObject.AddComponent<FixedJoint>(); }
			trackerConnection.connectedBody = kinematicObject;
		}

		public Rigidbody GetAttachedObject()
		{
			if (attachment != null)
			{
				return attachment.connectedBody;
			}
			else { return null; }
		}

		/// <summary>
		/// Used for holding objects in pre-defined positions relative to the controller.
		/// Re-attaches the attached Object, after matching their transform with the controller.
		/// </summary>
		public void RepositionAttachment()
		{
			if (attachment != null)
			{
				Rigidbody attachedObject = attachment.connectedBody;

				// Optionally turn it into a trigger until it's not colliding with anything else:
				var transition = attachedObject.GetComponent<TriggerColliderTransition>();
				if (transition != null)
				{
					transition.MakeTrigger();
					transition.makeColliderWhenFree = true;
				}

				// Reposition:
				attachment.connectedBody = null;
				attachedObject.transform.SetPositionAndRotation(transform.position, transform.rotation);
				attachment.connectedBody = attachedObject;
			}
		}

		public bool StartHold(Rigidbody target)
		{
			if (attachment == null)
			{
				colliderTransitions.MakeTrigger();

				attachment = gameObject.AddComponent<FixedJoint>();
				attachment.connectedBody = target;
				attachment.breakForce = target.mass * 1000;
				attachment.breakTorque = target.mass * 1000;
				attachment.enableCollision = false;
				attachment.enablePreprocessing = false;// leaving this enabled ends up freezing rotation.
				return true;
			}

			return false;
		}

		public void Drop()
		{
			if (attachment != null)
			{
				Destroy(attachment);
				colliderTransitions.makeColliderWhenFree = true;
			}
		}
      

    }
}