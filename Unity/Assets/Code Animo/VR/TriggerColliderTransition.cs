﻿// Copyright 2018 Laurens Mathot (Code Animo)
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using UnityEngine;

namespace CodeAnimo
{
	public class TriggerColliderTransition : MonoBehaviour
	{
		public bool makeColliderWhenFree = true;

		private Collider[] childColliders;

		public void Awake()
		{
			childColliders = GetComponentsInChildren<Collider>();
		}

		protected void FixedUpdate()
		{
			if (makeColliderWhenFree)
			{
				bool noCollisions = true;
				for (uint i = 0; i < childColliders.Length; ++i)
				{
					Bounds bounds = childColliders[i].bounds;
					Vector3 halfExtents = bounds.extents * 0.5f;
					if (Physics.CheckBox(bounds.center, halfExtents, Quaternion.identity, -1, QueryTriggerInteraction.Ignore))
					{
						noCollisions = false;
						break;
					}
				}

				if (noCollisions)
				{
					MakeCollider();
				}

			}
		}

		public void MakeTrigger()
		{
			for (uint j = 0; j < childColliders.Length; ++j)
			{
				childColliders[j].isTrigger = true;
			}
		}

		protected void MakeCollider()
		{
			for (uint j = 0; j < childColliders.Length; ++j)
			{
				childColliders[j].isTrigger = false;
			}
			makeColliderWhenFree = false;
		}



	}
}