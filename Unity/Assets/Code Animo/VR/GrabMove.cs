﻿// Copyright 2018 Laurens Mathot (Code Animo)
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using UnityEngine;

namespace CodeAnimo
{
    [RequireComponent(typeof(SteamVR_ControllerManager))]
	[RequireComponent(typeof(BoxCollider))]
    public class GrabMove : MonoBehaviour {
        enum Side
        {
            None,
            Left,
            Right
        };

        [Tooltip("Tracked head position.")]
        public Transform head;

        [Tooltip("Layers that we can stand on. E.g.: terrain and large objects.")]
        public LayerMask floorLayers;

		[Tooltip("Layers that we can't walk through.")]
		public LayerMask collisionLayers;

		[Tooltip("The object that will be made visible in the location where we're expecting to teleport to.")]
        public GameObject footIndicator;

		[Tooltip("Having the collider start a little bit higher makes it easier to climb onto ledges, and reduces hitting bumpy floors. High values can cause unpleasant sensations by letting you climb lower than your floor in real life.")]
		public float colliderFloorOffset = 0.1f;

		[Tooltip("Allows faster movement. Not having this at 1.0 seems like a recipe for more simulation sickness. Be mindful.")]
		public float movementMultiplier = 1.0f;

		[Tooltip("Crossing a grid point triggers a haptic tick.")]
		public float hapticGridSize = .05f;

		protected BoxCollider bodyCollider;
        protected Transform draggedView;

        private SteamVR_ControllerManager controllerManager;

        private HandInteractionPicker rightController;
        private HandInteractionPicker leftController;
        private HandInteractionPicker lastGrabbing = null;

        private Vector3 lastDragPosition;
		private Vector3 lastGridTick;
		private Vector3 collidedOfset;

        private float lastGripAmountRight = 0.0f;
        private float lastGripAmountLeft = 0.0f;

        private void Awake()
        {
            draggedView = GetComponent<Transform>();
			bodyCollider = GetComponent<BoxCollider>();
			bodyCollider.enabled = false;

            controllerManager = GetComponent<SteamVR_ControllerManager>();
            rightController = controllerManager.right.GetComponentInChildren<HandInteractionPicker>();
            leftController = controllerManager.left.GetComponentInChildren<HandInteractionPicker>();

			footIndicator.SetActive(false);
        }

        private void FixedUpdate()
        {
            float gripAmountLeft = leftController.walkInput;
            float gripAmountRight = rightController.walkInput;
            
            HandInteractionPicker grabbingController = null;

            bool rightWantsToDrag = gripAmountRight > 0.0f;            
            bool leftWantsToDrag = gripAmountLeft > 0.0f;

			// Disallow walking when still climbing:
			if (lastGrabbing != null && lastGrabbing.climbing && lastGrabbing.walkInput > 0.0f)
			{
				rightWantsToDrag &= rightController.climbing;
				leftWantsToDrag &= leftController.climbing;
			}

            if (rightWantsToDrag && leftWantsToDrag)
            {
                // Decide which controller is grabbing:
                if (gripAmountLeft > gripAmountRight)
                {
                    grabbingController = leftController;
                }
                else if (gripAmountRight > gripAmountLeft)
                {
                    grabbingController = rightController;
                }
                else
                {
                    // equal values, pick the one that reached it last
                    if (lastGripAmountLeft < lastGripAmountRight)
                    { grabbingController = leftController;  }
                    else if (lastGripAmountRight < lastGripAmountLeft)
                    { grabbingController = rightController;  }
                    else
                    { grabbingController = lastGrabbing; }
                }
            }
            else if (rightWantsToDrag)
            { grabbingController = rightController; }
            else if (leftWantsToDrag)
            { grabbingController = leftController; }

			if (lastGrabbing != null && lastGrabbing != grabbingController) { lastGrabbing.controllerGraphics.HideIndicators(); }

            if (grabbingController)
            {
                // Some controller is grabbing
                if (grabbingController != lastGrabbing)
                {
					if (!grabbingController.climbing) { TeleportToFloor(); }

                    lastDragPosition = grabbingController.transform.position;
					lastGridTick = draggedView.position;
					collidedOfset = Vector3.zero;
                }

                // Determine movement:
                Vector3 controllerPosition = grabbingController.transform.position;
                Vector3 dragMotion = lastDragPosition - controllerPosition;
				dragMotion *= movementMultiplier;

				// Resolve collision Compensation:
				Vector3 ignoredMotion = Vector3.zero;
				ignoredMotion.x = CancelToZero(dragMotion.x, collidedOfset.x);
				ignoredMotion.y = CancelToZero(dragMotion.y, collidedOfset.y);
				ignoredMotion.z = CancelToZero(dragMotion.z, collidedOfset.z);

				collidedOfset -= ignoredMotion;
				dragMotion -= ignoredMotion;
				

				// Get information about the floor
				RaycastHit hitInfo; bool hit;
                if (grabbingController.climbing)
				{
					grabbingController.controllerGraphics.ShowClimbMoveIndicator();
				}
                else
				{
					// Limit movement relative to the plane of the floor normal:
					hit = Physics.Raycast(controllerPosition, Vector3.down, out hitInfo, Mathf.Infinity, floorLayers.value);
					Vector3 clampDir = Vector3.up;
                    if (hit)
                    { clampDir = hitInfo.normal; }

                    Quaternion clampRotation = Quaternion.FromToRotation(clampDir, Vector3.up);
					Quaternion inverseRotation = Quaternion.Inverse(clampRotation);
                    dragMotion = clampRotation * dragMotion;
                    dragMotion.y = 0;
                    dragMotion = inverseRotation * dragMotion;

					grabbingController.controllerGraphics.ShowWalkMoveIndicator(inverseRotation);
				}
				
				// Limit motion to not go through colliders:
				float heightFromFloor = head.position.y - transform.position.y;
				float colliderFloorOffsetClamped = Mathf.Min(colliderFloorOffset);
				Vector3 colliderSize = bodyCollider.size;
				colliderSize.y = heightFromFloor - colliderFloorOffsetClamped;

				Vector3 bodyCenter = (head.position + dragMotion);
				bodyCenter.y -= heightFromFloor * 0.5f;
				bodyCenter.y += colliderFloorOffsetClamped;

				bodyCollider.size = colliderSize;
				bodyCollider.center = bodyCenter - transform.position;
				bodyCollider.enabled = true;

				Collider[] obstacles = Physics.OverlapBox(bodyCenter, 0.5f * bodyCollider.size, Quaternion.identity, collisionLayers.value, QueryTriggerInteraction.Ignore);
				Vector3 collisionResolution = Vector3.zero;
				for (int i = 0; i < obstacles.Length; i++)
				{
					Collider other = obstacles[i];
					Vector3 direction;
					float distance;
					bool overlap = Physics.ComputePenetration(bodyCollider, transform.position, transform.rotation, other, other.transform.position, other.transform.rotation, out direction, out distance);

					if (overlap)
					{
						Debug.DrawRay(bodyCenter, direction);
						collisionResolution += distance * direction;
					}
				}
				bodyCollider.enabled = false;

				// Apply movement:
				draggedView.position += dragMotion + collisionResolution;
				collidedOfset += collisionResolution;

				// Haptic Feedback:
				bool crossedX = Mathf.FloorToInt(lastGridTick.x / hapticGridSize) != Mathf.FloorToInt(draggedView.position.x / hapticGridSize);
				bool crossedY = Mathf.FloorToInt(lastGridTick.y / hapticGridSize) != Mathf.FloorToInt(draggedView.position.y / hapticGridSize);
				bool crossedZ = Mathf.FloorToInt(lastGridTick.z / hapticGridSize) != Mathf.FloorToInt(draggedView.position.z / hapticGridSize);
				if (crossedX) { lastGridTick.x = draggedView.position.x; }
				if (crossedY) { lastGridTick.y = draggedView.position.y; }
				if (crossedZ) { lastGridTick.z = draggedView.position.z; }
				if (crossedX || crossedY || crossedZ){ grabbingController.HapticPulse(900);	}


				lastDragPosition = grabbingController.transform.position;

				// Show where we'll end up on release:
				hit = Physics.Raycast(head.position, Vector3.down, out hitInfo, Mathf.Infinity, floorLayers.value);
                if (hit)
                {
					// Orient feet, relative to the world 'up'.
					Vector3 lookDirection = head.rotation * Vector3.up;
					Vector3 perpendicular = Vector3.Cross(lookDirection, Vector3.up);// Benefit of low FOV: you can't see your feet when lookdirection == Vector.up
					Vector3 horizontalDir = Vector3.Cross(Vector3.up, perpendicular);
					Quaternion feetDirection = Quaternion.FromToRotation(Vector3.forward, horizontalDir);

					footIndicator.transform.SetPositionAndRotation(hitInfo.point, feetDirection);
                    footIndicator.SetActive(true);
                }

            }
            else if (lastGrabbing)
            {
                // Just stopped grabbing this frame
                TeleportToFloor();
            }

            lastGrabbing = grabbingController;
            lastGripAmountLeft = gripAmountLeft;
            lastGripAmountRight = gripAmountRight;

        }

		protected void TeleportToFloor()
		{
			footIndicator.SetActive(false);

			RaycastHit hitInfo;
			bool hit = Physics.Raycast(head.position, Vector3.down, out hitInfo, Mathf.Infinity, floorLayers.value);
			if (hit)
			{
				Vector3 viewPos = draggedView.position;
				viewPos.y = hitInfo.point.y;
				draggedView.position = viewPos;
			}
		}

		private float CancelToZero(float target, float cancellation )
		{
			if (target > 0.0f && cancellation > 0.0f) { return target < cancellation ? target : cancellation; }
			else if (target < 0.0f && cancellation < 0.0f) { return target > cancellation ? target : cancellation; }
			return 0.0f;
		}


    }
}