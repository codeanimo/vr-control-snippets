﻿// Copyright 2018 Laurens Mathot (Code Animo)
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using UnityEngine;

namespace CodeAnimo
{
	[RequireComponent(typeof(Rigidbody))]
	public class HandInteractionPicker : MonoBehaviour {

		public ControllerGraphics controllerGraphics;
		public TrackerPhysics controllerPhysics;

		public string walkAxis = "LeftRightThumbPress";
		public string holdObjectAxis = "LeftRightGripSqueeze";
		public string lockHoldAxis = "LeftRightMenu";

		protected Rigidbody holdableTarget;

		[HideInInspector]
        public float walkInput = 0.0f;
		[HideInInspector]
        public bool climbing = false;
		protected int climbableCount = 0;

		protected bool isObjectLocked = false;
		protected bool holdButtonWasDown = false;
		protected bool lockButtonWasDown = false;

		private SteamVR_TrackedObject trackedObject;

		private void OnEnable()
		{
			controllerPhysics.ConnectToTracker(GetComponent<Rigidbody>());
			trackedObject = GetComponent<SteamVR_TrackedObject>();
		}

		private void OnTriggerEnter(Collider other)
        {
            Climbable climbable = other.GetComponent<Climbable>();
            if (climbable)
            {
                ++climbableCount;
            }


            // Targeting objects we might want to hold.
            if (!holdableTarget)
            {
                Rigidbody target = other.GetComponentInParent<Rigidbody>();
                if (target != null && !target.isKinematic)
				{
					holdableTarget = target;
				}
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (climbableCount > 0)
            {
                if (other.GetComponentInParent<Climbable>())
                {
                    --climbableCount;
                }
            }

            // untargeting objects we might want to hold:
            if (holdableTarget)
            {
                Rigidbody target = other.GetComponentInParent<Rigidbody>();
                if (target != null)
                {
                    if (target == holdableTarget)
                    {
						holdableTarget = null;
                    }
                }
            }
        }

		/// <summary>
		/// Trigger a haptic pulse for the connected controller
		/// </summary>
		/// <param name="usDuration">Strength in microseconds. Vive Controller Range: 0-3999</param>
		/// <param name="axis">Only axis 0 is exposed for the Vive controller</param>
		public void HapticPulse(ushort usDuration, uint axis = 0)
		{
			var system = Valve.VR.OpenVR.System;
			if (system != null){ system.TriggerHapticPulse((uint)trackedObject.index, axis, (char)usDuration); }
		}

		private void FixedUpdate()
        {
            walkInput = Input.GetAxisRaw(walkAxis);
            climbing = climbableCount > 0 && walkInput > 0.0f;

			bool holdButtonIsDown = Input.GetAxisRaw(holdObjectAxis) > 0.0f;
			bool lockButtonIsDown = Input.GetAxisRaw(lockHoldAxis) > 0.0f;

			bool startedHolding = false;

			bool holdingAnObject = controllerPhysics.GetAttachedObject() != null;
			if (!holdingAnObject) { isObjectLocked = false; }


			if (holdButtonIsDown)
			{
				if (!holdingAnObject && holdableTarget != null)
				{
					holdingAnObject = controllerPhysics.StartHold(holdableTarget);
					startedHolding = true;
				}
			}
			else if ( holdingAnObject && !isObjectLocked ){ controllerPhysics.Drop(); }

			if (lockButtonIsDown && (!lockButtonWasDown || startedHolding))
			{
				if (isObjectLocked) { isObjectLocked = false; }
				else 
				{
					controllerPhysics.RepositionAttachment();
					isObjectLocked = true;
				}
			}

			holdButtonWasDown = holdButtonIsDown;
			lockButtonWasDown = lockButtonIsDown;

        }


    }
}