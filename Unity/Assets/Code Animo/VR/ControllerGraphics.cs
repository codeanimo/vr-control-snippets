﻿// Copyright 2018 Laurens Mathot (Code Animo)
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using UnityEngine;

namespace CodeAnimo
{
	public class ControllerGraphics : MonoBehaviour {

		public MeshRenderer[] subModels;
		public Material seeThrough;
		public Material highlight;

		public GameObject walkMoveIndicator;
		public GameObject climbMoveIndicator;
		private SteamVR_RenderModel attachedModel;

		public enum ModelIndex
		{
			Body = 0,
			MenuButton,
			LED,
			LeftGrip,
			RightGrip,
			ScrollWheel,
			Status,
			SystemButton,
			Trackpad,
			TrackpadScrollCutout,
			TrackpadTouch,
			Trigger,
			Count
		}

		private SteamVR_Events.Action renderModelLoadedAction;

        void Awake()
        {
			HideIndicators();

			attachedModel = GetComponent<SteamVR_RenderModel>();
			renderModelLoadedAction = SteamVR_Events.RenderModelLoadedAction(OnRenderModelLoaded);
        }

        void OnEnable()
        {
            if (renderModelLoadedAction != null) {renderModelLoadedAction.enabled = true;}
        }

        void OnDisable()
        {
			if (renderModelLoadedAction != null) { renderModelLoadedAction.enabled = false; }
        }

        void OnRenderModelLoaded(SteamVR_RenderModel model, bool action)
        {
			if (attachedModel && attachedModel == model)
			{
				this.subModels = model.GetComponentsInChildren<MeshRenderer>(true);

				if (this.subModels.Length != (int)ModelIndex.Count)
				{ Debug.LogWarning("Unexpected number of models found,(" + this.subModels.Length + "), expected " + (int)ModelIndex.Count + ", indices might have changed since this script was written.", this); }

				seeThrough.mainTexture = this.subModels[0].material.mainTexture;

				for (int i = 0; i < this.subModels.Length; i++)
				{
					this.subModels[i].material = seeThrough;
				}

				subModels[(int)ModelIndex.Trigger].material = highlight;

			}

		}

		public void ShowWalkMoveIndicator(Quaternion orientation)
		{
			if (walkMoveIndicator != null)
			{
				walkMoveIndicator.transform.rotation = orientation;
				walkMoveIndicator.SetActive(true);
			}
		}

		public void ShowClimbMoveIndicator()
		{
			if (climbMoveIndicator != null)
			{
				climbMoveIndicator.SetActive(true);
			}
		}

		public void HideIndicators()
		{
			if (walkMoveIndicator != null) { walkMoveIndicator.SetActive(false); }
			if (climbMoveIndicator != null) { climbMoveIndicator.SetActive(false); }
		}


	}
}